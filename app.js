const express = require("express");
const app = express();
const BoardController = require("./BoardController");
const DefinedNumberRule = require("./Rules/DefinedNumberRule");
const MultipleRule = require("./Rules/MultipleRule");
const HotelRule = require("./Rules/HotelRule");
const MazeRule = require("./Rules/MazeRule");
const WellRule = require("./Rules/WellRule");
const PrisonRule = require("./Rules/PrisonRule");
const DefaultRule = require("./Rules/DefaultRule");
const DeathRule = require("./Rules/DeathRule");
const FinishRule = require("./Rules/FinishRule");
const PunishingRule = require("./Rules/PunishingRule");
const Output = require('./Output');

app.listen(() => {
  newBoard.travelTheBoard(new Output());
});

const rules = [
  new DefinedNumberRule(6), 
  new PrisonRule(),
  new HotelRule(),
  new MazeRule(),
  new WellRule(),
  new PunishingRule(),
  new MultipleRule(6),
  new DeathRule(),
  new FinishRule(),
  new DefaultRule(),
];

const newController = new BoardController()
const newBoard = newController.inicializeBoard(80, rules)