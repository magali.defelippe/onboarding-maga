const Position = require("./Position");
const Board = require("./Board");

class BoardController {

    makePositions(totalPositions, rules) {
        let positionsInArray = [];
        for (let position = 1; position <= totalPositions ; position++) {
            const individualPosition = new Position(
                position,
                this.makeRules(position, rules)
            );
            positionsInArray.push({
                position: individualPosition.number,
                rule: individualPosition.rule,
            });
        }
        return positionsInArray;
    }

    verifyTypeOfValue(number){
        const validType = /^[0-9]+$/;
        const characterToString = number.toString();
        if(characterToString.match(validType)){
            return true;
        }else {
            return false;
        }
    }
    
    makeRules(number, rules) {
        let ruleForThePosition = rules.find(
            (rule) => rule.checkTheRule(number)
        );
        return ruleForThePosition.getMessage();
    }

    inicializeBoard(totalPositions, rules) {
        const validate = this.verifyTypeOfValue(totalPositions);
        if(validate){
            const positions = this.makePositions(totalPositions, rules)
            return new Board(positions);
        }else{
            return 'Numero no valido';
        }
    }
}

module.exports = BoardController;
