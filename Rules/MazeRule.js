class MazeRule {
    constructor() {
        this.rule = "The Maze: Go back to space 39";
    }
  
    checkTheRule(number) {
      if ( number === 42 ) return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = MazeRule;