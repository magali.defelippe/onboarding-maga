class MultipleRule {
    constructor(referenceNumber) {
        this.rule =  "Move two spaces forward.";
        this.referenceNumber = referenceNumber;
    }
  
    checkTheRule(number) {
      if (number % this.referenceNumber === 0)
        return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = MultipleRule;