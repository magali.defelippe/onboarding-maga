class DefaultRule{
    constructor(){
        this.rule = null;
    }

    checkTheRule(number){
        this.rule = `Stay in space ${number}`;
        return true;
    }

    getMessage(){
        return this.rule;
    }
}

module.exports = DefaultRule;