class DeathRule {
  constructor() {
    this.rule = "Death: Return your piece to the beginning - start the game again";
  }

  checkTheRule(number) {
    if (number === 58) return true;
  }

  getMessage() {
    return this.rule;
  }
}

module.exports = DeathRule;
