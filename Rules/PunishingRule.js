class PunishingRule {
    constructor() {
      this.rule = "Move to space 53 and stay in prison for two turns";
    }
  
    checkTheRule(number) {
      if (number > 63) return true;
    }
  
    getMessage() {
      return this.rule;
    }
  }
  
module.exports = PunishingRule;
  