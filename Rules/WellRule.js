class WellRule {
    constructor() {
        this.rule = "The Well: Wait until someone comes to pull you out - they then take your place";
    }
  
    checkTheRule(number) {
      if ( number === 31 ) return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = WellRule;