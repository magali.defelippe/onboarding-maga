class PrisonRule {
    constructor() {
        this.rule = "The Prison: Wait until someone comes to release you - they then take your place";
    }
  
    checkTheRule(number) {
      if ( number >= 50 && number <= 55) return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = PrisonRule;