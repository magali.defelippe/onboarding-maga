const PunishingRule = require('../PunishingRule');

describe("Punishing rule use case should", () => {
    const useCase = new PunishingRule();

    test("Return true ", () => {
      expect(useCase.checkTheRule(64)).toEqual(true);
      expect(useCase.checkTheRule(115)).toEqual(true);
    });
  
    test("Return the sentence", () => {
      expect(useCase.getMessage()).toEqual('Move to space 53 and stay in prison for two turns');
    });
});
  