const WellRule = require("../WellRule");

describe("Well rule use case should", () => {

  test("Return true ", async () => {
    const useCase = new WellRule();
    expect(useCase.checkTheRule(31)).toEqual(true);
  });

  test("Return the sentence ", async () => {
    const useCase = new WellRule();
    expect(useCase.getMessage()).toEqual("The Well: Wait until someone comes to pull you out - they then take your place");
  });
});
