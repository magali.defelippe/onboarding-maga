const FinishRule = require('../FinishRule');

describe("Finish rule use case should", () => {
    const useCase = new FinishRule();

    test("Return true ", () => {
      expect(useCase.checkTheRule(63)).toEqual(true);
    });
  
    test("Return the sentence", () => {
      expect(useCase.getMessage()).toEqual('Finish: you ended the game');
    });
  });
  