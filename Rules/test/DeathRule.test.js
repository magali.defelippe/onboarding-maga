const DeathRule = require('../DeathRule');

describe("Death rule use case should", () => {
    const useCase = new DeathRule();

    test("Return true ", async () => {
      expect(useCase.checkTheRule(58)).toEqual(true);
    });
  
    test("Return the sentence", async () => {
      expect(useCase.getMessage()).toEqual('Death: Return your piece to the beginning - start the game again');
    });
  });
  