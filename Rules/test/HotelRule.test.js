const HotelRule = require("../HotelRule");

describe("Hotel rule use case should", () => {

  test("Return true ", async () => {
    const useCase = new HotelRule();
    expect(useCase.checkTheRule(19)).toEqual(true);
  });

  test("Return the sentence ", async () => {
    const useCase = new HotelRule();
    expect(useCase.getMessage()).toEqual("The Hotel: Stay for (miss) one turn");
  });
});
