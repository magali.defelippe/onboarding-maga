const MultipleRule = require("../MultipleRule");

describe("Multiple rule use case should", () => {

  test("Return true ", async () => {
    const useCase = new MultipleRule(6);
    expect(useCase.checkTheRule(6)).toEqual(true);
  });

  test("Return the sentence ", async () => {
    const useCase = new MultipleRule(6);
    expect(useCase.getMessage()).toEqual("Move two spaces forward.");
  });
});
