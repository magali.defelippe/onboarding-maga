const DefinedNumberRule = require("../DefinedNumberRule");


describe("Define rule use case should", () => {

  test("Return true ", async () => {
    const useCase = new DefinedNumberRule(6);
    expect(useCase.checkTheRule(6)).toEqual(true);
  });

  test("Return the sentence ", async () => {
    const useCase = new DefinedNumberRule(6);
    expect(useCase.getMessage()).toEqual("The Bridge: Go to space 12");
  });


});
