const PrisonRule = require("../PrisonRule");

describe("Prison rule use case should", () => {

  test("Return true if the number is 50", async () => {
    const useCase = new PrisonRule();
    expect(useCase.checkTheRule(50)).toEqual(true);
  });

  test("Return the sentence", async () => {
    const useCase = new PrisonRule();
    expect(useCase.getMessage()).toEqual("The Prison: Wait until someone comes to release you - they then take your place");
  });

});
