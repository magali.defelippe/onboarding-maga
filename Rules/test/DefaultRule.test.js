const DefaultRule = require("../DefaultRule");

describe("Default rule use case should", () => {

  test("Return true ", async () => {
    const useCase = new DefaultRule();
    expect(useCase.checkTheRule(31)).toEqual(true);
  });

  test("Return the sentence", async () => {
    const useCase = new DefaultRule();
    useCase.checkTheRule(6)
    expect(useCase.getMessage()).toEqual('Stay in space 6');
  });
});
