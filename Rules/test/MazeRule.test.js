const MazeRule = require("../MazeRule");

describe("Maze rule use case should", () => {

  test("Return true", async () => {
    const useCase = new MazeRule();
    expect(useCase.checkTheRule(42)).toEqual(true);
  });

  test("Return the sentence ", async () => {
    const useCase = new MazeRule();
    expect(useCase.getMessage()).toEqual("The Maze: Go back to space 39");
  });
});
