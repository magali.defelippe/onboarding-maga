class DefinedNumberRule {
    constructor(referenceNumber) {
        this.rule = "The Bridge: Go to space 12";
        this.referenceNumber = referenceNumber;
    }
  
    checkTheRule(number) {
      if (number === this.referenceNumber) return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = DefinedNumberRule;
  