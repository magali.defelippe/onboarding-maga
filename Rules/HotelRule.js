class HotelRule {
    constructor() {
        this.rule = "The Hotel: Stay for (miss) one turn";
    }
  
    checkTheRule(number) {
      if ( number === 19 ) return true;
    }

    getMessage(){
      return this.rule;
    }
  }
  
  module.exports = HotelRule;