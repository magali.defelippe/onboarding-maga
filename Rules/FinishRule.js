class FinishRule {
    constructor() {
      this.rule = "Finish: you ended the game";
    }
  
    checkTheRule(number) {
      if (number === 63) return true;
    }
  
    getMessage() {
      return this.rule;
    }
  }
  
module.exports = FinishRule;
  