const Output = require("../Output");

describe("output use case should", () => {
  test("Return the output from parameters", () => {
    const position = { position: 2, rule: "Stay in space 2" };
    const useCase = new Output();

    expect(useCase.returnOutput(position)).toEqual(console.log(position));
  });
});
