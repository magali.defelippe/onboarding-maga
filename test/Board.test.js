const Board = require("../Board");

describe("board use case should", () => {
  const position1 = getMockedPosition(1, "Position 1");
  const position2 = getMockedPosition(2, "Position 2");
 
  const mockOutputReturnOutput = jest.fn().mockReturnValue(console.log('j'));
  const output = {
    returnOutput: mockOutputReturnOutput,
  };

  const positions = [position1, position2];


  beforeEach(() => {
    jest.clearAllMocks();
  })

  test("Travel the board", () => {
    const useCase = new Board(positions);
    useCase.travelTheBoard(output);

    expect(output.returnOutput).toBeCalledTimes(2);
  });

  function getMockedPosition(position, rule) {
    return {
      position,
      rule,
    };
  }
});
