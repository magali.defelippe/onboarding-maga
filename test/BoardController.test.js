const BoardController = require("../BoardController");


describe("board controller use case should", () => {

  const numberTenRule = getMockedRule(number => number === 10, 'Position 10');
  const defaultRule = getMockedRule(() => true, 'Default message');

  const rules = [ numberTenRule, defaultRule ];

  const totalPositions = 63;

  const useCase = new BoardController();

  beforeEach(() => {
    jest.clearAllMocks();
  })
  
  test("Initialize a board", () => {
    useCase.inicializeBoard(totalPositions, rules);
    expect(numberTenRule.checkTheRule).toHaveBeenCalledTimes(totalPositions);
    expect(defaultRule.checkTheRule).toHaveBeenCalledTimes(totalPositions - 1);
    expect(numberTenRule.getMessage).toHaveBeenCalledTimes(1);
    expect(defaultRule.getMessage).toHaveBeenCalledTimes(totalPositions - 1);
  })

  function getMockedRule(checkTheRule, message) {
    const mockDefaultRuleCheckTheRule = jest.fn().mockImplementation(checkTheRule);
    const mockDefaultRuleGetMessage = jest.fn().mockReturnValue(message);
    return {
      checkTheRule: mockDefaultRuleCheckTheRule,
      getMessage: mockDefaultRuleGetMessage,
    };
  }
});
