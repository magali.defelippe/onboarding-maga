
class Board {
  constructor(positions) {
    this.positions = positions;
  }

  travelTheBoard(output) {
    for (let position in this.positions) {
     output.returnOutput(this.positions[position])
    }
  }
}

module.exports = Board;
